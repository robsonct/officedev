app.controller("relConfidenciaBaixa", ['$scope', '$timeout', '$http', '$filter', '$compile', function ($scope, $timeout, $http, $filter, $compile) {

    $scope.dadosConfidenciaBaixa = [];
    $scope.mostraLoader = false;

    var elmtDt = $('#tableConfidencia');

    function comboGenerate(arr, idForm, exemplo) {
        var combo = '';
        var length = 0;
        if (arr.length <= 0) return combo;
        if (arr[0].confidence > 0.85) return combo;
        length = arr.length;
        if (length >= 10) length = 10;

        combo += '<form name="form_' + idForm + '" id="form_' + idForm + '" method="POST" class="formClick" ng-submit="formExemploSubmit($event, ' + idForm + ')">';
        combo += '<div class="col-xs-3 col-sm-4 col-md-4">';
        combo += '<input type="hidden" name="example_' + idForm + '" id="example_' + idForm + '" value="' + exemplo + '">';
        combo += '<select class="form-control" id="intent_' + idForm + '" name="intent_' + idForm + '">';
        for (var i = 0; i < length; i++) {
            combo += '<option value="' + arr[i].intent + '">#' + arr[i].intent + '</option>';
        }
        combo += '</select>';
        combo += '</div>';
        combo += '<div class="col-xs-3 col-sm-4 col-md-4">';
        combo += '<button class="btn btn-primary btn-block" type="submit" id="btnAddExemplo_' + idForm + '" name="btnAddExemplo_' + idForm + '">Adicionar Exemplo</button>';
        combo += '</div>';
        combo += '</form>';
        return combo;
    }

    $scope.formExemploSubmit = function (obj, idForm) {
        var obj = {
            intent: $('#intent_' + idForm).val(),
            nameInputIntent: 'intent_' + idForm,
            example: $('#example_' + idForm).val(),
            nameInputExample: 'example_' + idForm
        }

        obj.example = prompt("Adicionar Exemplo", obj.example);

        if (obj.intent.trim() != '' && obj.example.trim() != '') {
            var btn = $('#btnAddExemplo_' + idForm);
            btn.addClass('disabled');
            btn.off('click');
            $http({
                method: 'POST',
                url: '/intent/saveExample',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: obj
            })
                .then(function (success) {
                }, function (err) {
                    $scope.sessionExpired(err);
                });
        }
    }

    function setInputDate() {
        var hoy = new Date(),
            d = hoy.getDate(),
            m = hoy.getMonth(),
            y = hoy.getFullYear();
        $scope.date_start = new Date(y, m, d);
        $scope.date_end = new Date(y, m, d);
    };

    $scope.load = function () {
        $scope.mostraLoader = true;
        setInputDate();
        var start = new Date($scope.date_start);
        start.setUTCHours(0, 0, 0, 0);
        var end = new Date($scope.date_end);
        end.setUTCHours(23, 59, 59, 0);
        $scope.threshold = 0.65;
        $scope.mostraLoader = false;
    }

    $scope.formSubmit = function () {
        var start = new Date($scope.date_start);
        start.setUTCHours(0, 0, 0, 0);
        var end = new Date($scope.date_end);
        end.setUTCHours(23, 59, 59, 0);


        var req = {
            method: 'POST',
            url: '/relatorioconfidenciabaixa/find',
            headers: {
                'Content-Type': 'application/json'
            },
            data: {
                busca: {
                    maiorIgual: start,
                    menorIgual: end,
                    nivelConfidencia: $scope.threshold
                }
            }
        };

        $scope.mostraLoader = true;
        $http(req)
            .then(function (success) {
                if (success.data.status == 'ok') {
                    var res = success.data.result;
                    if ($.fn.DataTable.fnIsDataTable(elmtDt)) {
                        elmtDt.DataTable().destroy();
                        $scope.dadosConfidenciaBaixa = [];
                    }
                    console.log(res);
                    $scope.dadosConfidenciaBaixa = res;
                }
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            }).then(function () {
                $scope.mostraLoader = false;
                $timeout(function () {
                    elmtDt.DataTable({
                        responsive: true,
                        lengthMenu: [[10, 30, 50, -1], [10, 30, 50, "All"]],
                        pageLength: 10,
                        destroy: true,
                        order: [[5, "desc"]],
                        columnDefs: [
                            { "width": "5%", "targets": 0 },
                            { "width": "28%", "targets": 1 },
                            { "width": "22%", "targets": 2 },
                            { "width": "20%", "targets": 3 },
                            { "width": "10%", "targets": 4 },
                            { "width": "15%", "targets": 5 }
                        ],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'pdf'
                        ]
                    });
                }, 250);
            });

    }


    $scope.abrilModal = function (workspace_id, conversation_id, id, event) {
        var req = {
            method: 'GET',
            cache: false,
            url: '/historico/find?conversationId=' + conversation_id + '&workspaceId=' + workspace_id,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        $http(req)
            .then(function (success) {
                var dados = success.data;
                if (dados.status == 'ok') {
                    $scope.mworkspace = workspace_id;
                    $scope.mconversation = conversation_id;

                    var template = '<tr class="CLASSLINE"><td class="text-left modalHistoricoAgente">USER</td><td>TEXT INTENCAO</td></tr>';
                    var htmlDialogo = '';
                    var intencoes = '';
                    var countForm = 0;

                    for (var i = 0; i < dados.body.length; i++) {
                        countForm++;
                        intencoes = '';
                        var item = dados.body[i];

                        if (item.typeAgent == 1) {
                            angular.forEach(item.intents, function (v, c) {
                                if (c <= 2) {
                                    var confidencia = (v.confidence * 100).toFixed(2);
                                    intencoes += '&nbsp;#' + v.intent + ':&nbsp;' + confidencia + '%&nbsp;';
                                }
                            });
                            intencoes = intencoes.trim() == '' ? '' : '<span class="intent"><img src="/images/intention.png" height="10" alt="Intenções">&nbsp;' + intencoes.trim() + '</span>';

                            htmlDialogo += template
                                .replace('USER', 'Watson')
                                .replace('TEXT', item.text.trim())
                                .replace('CLASSLINE', 'modalHistoricoLinhaAtendente')
                                .replace('INTENCAO', intencoes);
                        } else {
                            if ((i + 1) < dados.body.length) {
                                var nextItem = dados.body[i + 1];
                                intencoes = comboGenerate(nextItem.intents, countForm, item.text.trim());
                                intencoes = intencoes.trim() == '' ? '' : '<span class="addIntent">' + intencoes + '</span>';
                            }
                            htmlDialogo += template
                                .replace('USER', item.username)
                                .replace('TEXT', item.text.trim())
                                .replace('CLASSLINE', 'modalHistoricoLinhaUsuario')
                                .replace('INTENCAO', intencoes);
                        }
                    }

                    angular.element(document.querySelector('#conteudoDialogoModal')).html(htmlDialogo);
                    $compile($('#modalConversa'))($scope);


                }
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                angular.element(document.querySelector('#conteudoDialogoModal')).html('');
                $scope.sessionExpired(err);
            })
            .then(function () {
                $timeout(function () {
                    var elmt = angular.element(document.querySelector('#conteudoModalConversa'));
                    elmt.mCustomScrollbar({
                        setHeight: 550,
                        theme: "inset-2-dark"
                    });
                    elmt.mCustomScrollbar("update").mCustomScrollbar('scrollTo', 'bottom', {
                        scrollInertia: 10,
                        timeout: 0
                    });
                }, 250);
            });

    }

}]);