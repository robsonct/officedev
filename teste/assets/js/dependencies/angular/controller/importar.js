app.controller("importar", ['$scope', '$http', function ($scope, $http) {


    $scope.mostraLoader = false;

    $scope.load = function () {
        var hoje = new Date(),
            d = hoje.getDate(),
            m = hoje.getMonth(),
            y = hoje.getFullYear();


        $scope.date_start = new Date(y, m, d);
        $scope.date_end = new Date(y, m, d);
        $scope.inProgress = false;

        let req = {
            method: 'GET',
            cache: false,
            url: '/configconversation/load',
            headers: {
                'Content-Type': 'application/json'
            }
        };
        $http(req)
            .then(function success(res) {
                let data = res.data.body;
                angular.forEach(data, function (value, key) {
                    $scope.url_api = value.url_api;
                    $scope.user_bluemix = value.user;
                    $scope.token_bluemix = value.token;
                    $scope.workspace_id = value.workspace_id;
                });
            }, function fail(err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            });
    };

    $scope.formSubmit = function () {
        $scope.mostraLoader = true;
        if (!$scope.inProgress) {
            $scope.inProgress = true;
            var req = {
                method: 'POST',
                cache: false,
                url: '/importar/find',
                timeout: 500000,
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    url_api: $scope.url_api,
                    user_bluemix: $scope.user_bluemix,
                    token_bluemix: $scope.token_bluemix,
                    workspace_id: $scope.workspace_id,
                    date_start: $scope.date_start,
                    date_end: $scope.date_end
                }
            };
            $http(req)
                .then(function (success) {
                    $scope.inProgress = false;
                    var obj = success.data;
                    if (obj.status == 'ok') {
                        alert('dados ok');
                    }
                    $scope.mostraLoader = false;
                }, function (err) {
                    alert(JSON.stringify(err));
                    console.log(err);
                    $scope.sessionExpired(err);
                    $scope.mostraLoader = false;
                });
        };
    }
}]);