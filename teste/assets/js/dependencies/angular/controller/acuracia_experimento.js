app.controller("acuraciaExperimento", ['$scope', 'ConsTest', '$timeout', '$http', function ($scope, ConsTest, $timeout, $http) {

    $scope.listaTestes = [];
    $('#alertNovoTeste').hide();
    var elmtDt = $('#listaExperimento');
    var elmtDtMatriz = $('#tabelaMatrizConfusao');
    var elmtDtVetor = $('#tabelaVetorConfusao');

    $scope.mostraLoader = true;
    $scope.mostraLoaderVetor = true;
    $scope.tstMatriz = [];
    $scope.tstVetor = [];
    $scope.dataTeste = Date.now();
    $scope.pizzaSumario = [];

    function refreshResultado() {
        $scope.mostraLoader = true;
        if ($.fn.DataTable.fnIsDataTable(elmtDt)) {
            elmtDt.DataTable().destroy();
            $scope.listaTestes = [];
        }

        var req = {
            method: 'GET',
            cache: false,
            url: '/acuraciaexperimento/list',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        $http(req)
            .then(function (success) {
                var objLista = [];

                if (success.data.status == 'ok') {
                    $scope.listaTestes = success.data.body;
                }
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            }).then(function () {
                $scope.mostraLoader = false;
                $timeout(function () {
                    elmtDt.DataTable({
                        responsive: true,
                        lengthMenu: [
                            [10, 30, 50, -1],
                            [10, 30, 50, "All"]
                        ],
                        pageLength: 10,
                        destroy: true,
                        dom: 'Bfrtip',
                        buttons: [
                            'csv', 'pdf'
                        ]
                    });
                }, 250);
            });
    }

    $scope.load = refreshResultado();

    $scope.criarTeste = function () {
        $('#novoTeste').addClass('disabled');
        $('#alertNovoTeste').show();
        var req = {
            method: 'GET',
            cache: false,
            url: '/acuraciaexperimento/execute',
            headers: {
                'Content-Type': 'application/json'
            }
        };

        $http(req)
            .then(function (success) {
                refreshResultado();
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            })
            .then(function () {
                $('#novoTeste').removeClass('disabled');
                $('#alertNovoTeste').hide();
            });
    }


    $scope.gerarGraficoSumario = function (arraySummaryColors) {
        var ctx = document.getElementById('graphSummaryPizza').getContext('2d');
        var graphSummaryPizza = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: $scope.pizzaSumario,
                    backgroundColor: [
                        window.chartColors.blue,
                        window.chartColors.yellow,
                        window.chartColors.red
                    ],
                    label: 'Feedback Geral'
                }],
                labels: [
                    'Bom',
                    'Atenção',
                    'Perigo'
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Sumário'
                }
            }
        });

        var arraySummaryIntents = $scope.tstSumario.map(function (e) { return e.total });
        var arraySummaryLabel = $scope.tstSumario.map(function (e) { return e.intent });

        var ctx = document.getElementById('graphSummaryBar').getContext('2d');

        var feedbackBarChart = new Chart(ctx, {
            type: 'bar',
            data: {
                datasets: [{
                    data: arraySummaryIntents,
                    backgroundColor: arraySummaryColors,
                    label: 'Lista de Intenções'
                }],
                labels: arraySummaryLabel
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Feedback Geral'
                },
                scales: {
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            min: 0,
                            autoSkip: false
                        }
                    }]
                }
            }
        });
    }

    $scope.gerarGraficoAcuracia = function () {
        var ctx = document.getElementById('graphAccuracyClassPizza').getContext('2d');
        var graphAccuracyPizza = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: $scope.pizzaAcuracia,
                    backgroundColor: [
                        window.chartColors.blue,
                        window.chartColors.yellow,
                        window.chartColors.red
                    ],
                    label: 'Feedback Geral'
                }],
                labels: [
                    'Bom',
                    'Atenção',
                    'Perigo'
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Sumário'
                }
            }
        });
        var ctx = document.getElementById('graphAccuracyClassBar').getContext('2d');
        var corAcuraciaBom = [];
        var corAcuraciaAtencao = [];
        var corAcuraciaPerigo = [];
        var arrAcuracia = [];
        var bubbleColors = []

        var dadosAcuracia = $scope.tstAcuracia.map(function (e) {
            if (e.classAccuracy === 'danger')
                bubbleColors.push(window.chartColors.red, )
            if (e.classAccuracy === 'warning')
                bubbleColors.push(window.chartColors.yellow, )
            if (e.classAccuracy === 'success')
                bubbleColors.push(window.chartColors.blue, )
            return {
                nome: e.intent,
                x: e.total,
                y: e.threshold / e.total * 100,
                r: 5
            }
        })



        var graphAccuracyBubble = new Chart(ctx, {
            type: 'bubble',
            data: {
                datasets: [{
                    data: dadosAcuracia,
                    backgroundColor: bubbleColors
                }
                ]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    callbacks: {
                        // Use the footer callback to display the sum of the items showing in the tooltip
                        footer: function (tooltipItems, data) {
                            return data.datasets[0].data[tooltipItems[0].index].nome;
                        },
                    },
                    footerFontStyle: 'normal'
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            max: $scope.tstSumario[0].total + 1,
                            min: 0,
                        },
                        type: 'linear',
                        position: 'bottom'
                    }],
                    yAxes: [{
                        ticks: {
                            max: 100,
                            min: 0,
                        },
                        type: 'linear',
                        position: 'bottom'
                    }]
                }
            }
        });


    }

    $scope.gerarPrecisaoTotal = function () {
        var ctx = document.getElementById('graphPrecisaoPizza').getContext('2d');
        var graphSummaryPizza = new Chart(ctx, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        100 * ($scope.examplesOk / $scope.examplesTotal),
                        100 * (1 - ($scope.examplesOk / $scope.examplesTotal))
                    ],
                    backgroundColor: [
                        window.chartColors.blue,
                        window.chartColors.red
                    ],
                    label: 'Precisão Geral'
                }],
                labels: [
                    'Acertos',
                    'Erros'
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Sumário'
                }
            }
        });

    }

    $scope.loadSummary = function () {

        const createdAt = $scope.createdAt;
        var req = {
            method: 'GET',
            cache: false,
            url: '/acuraciaexperimento/sumario?createdAt=' + createdAt,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        $http(req)
            .then(function (success) {
                $scope.tstSumario = success.data.body[0].resultado.intentsList.map(function (e) { return e });
                $scope.tstAcuracia = success.data.body[0].resultado.intentsList.map(function (e) { return e });
                $scope.tstSumario.sort((a, b) => { return b.total - a.total });
                $scope.tstAcuracia.sort((a, b) => { return (b.threshold / b.total) - (a.threshold / a.total) });
                $scope.pizzaSumario = [0, 0, 0];
                $scope.pizzaAcuracia = [0, 0, 0];
                $scope.examplesTotal = 0;
                $scope.examplesOk = 0;
                const quartileSummary = quartile($scope.tstSumario);
                const quartileAccuracy = quartile($scope.tstAcuracia);

                var arraySummaryColors = [];

                $scope.tstSumario.forEach(function (e) {
                    // se acurácia for menor que a média, então é amarelo
                    // só é vermelho se menor que o lower q e número de intenções for alto
                    $scope.examplesTotal += e.total;
                    $scope.examplesOk += e.threshold;
                    if ((e.threshold / e.total) < (quartileAccuracy[2].threshold / quartileAccuracy[2].total)) {
                        e.classAccuracy = 'danger';
                        $scope.pizzaAcuracia[2]++;
                    } else
                        if ((e.threshold / e.total) < (quartileAccuracy[1].threshold / quartileAccuracy[1].total)) {
                            e.classAccuracy = 'warning';
                            $scope.pizzaAcuracia[1]++;
                        }
                        else {
                            e.classAccuracy = 'success';
                            $scope.pizzaAcuracia[0]++;
                        }

                    if (e.total >= quartileSummary[0].total) {
                        e.classSummary = 'success';
                        $scope.pizzaSumario[0]++;
                        arraySummaryColors.push(window.chartColors.blue);
                    }
                    else if (e.total >= quartileSummary[2].total) {
                        e.classSummary = 'warning';
                        $scope.pizzaSumario[1]++;
                        arraySummaryColors.push(window.chartColors.yellow);
                    }
                    else {
                        e.classSummary = 'danger';
                        $scope.pizzaSumario[2]++;
                        arraySummaryColors.push(window.chartColors.red);
                    }
                });

                $scope.gerarGraficoSumario(arraySummaryColors);
                $scope.gerarGraficoAcuracia();
                $scope.gerarPrecisaoTotal();


            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            })

    }
    $scope.loadExemplos = function (createdAt) {
        var req = {
            method: 'GET',
            cache: false,
            url: '/acuraciaexperimento/exemplos?createdAt=' + createdAt,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        $http(req)
            .then(function (success) {
                $scope.exemplosTotais = success.data.body;
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            })

    }


    $scope.adicionaExemplos = function (intent) {
        let retorno = [];
        if (typeof $scope.exemplosTotais === 'object')
            $scope.exemplosTotais.forEach(function (e) {
                if (intent === e.intent)
                    retorno = e.examples;
            });
        return retorno;

    }


    $scope.adicionaErros = function (deveria, foi) {
        let retorno = [];
        if (typeof $scope.exemplosTotais === 'object')
            $scope.exemplosTotais.forEach(function (e) {
                if (deveria === e.intent)
                    retorno = e.erros[foi];
            });
        return retorno;

    }

    $scope.loadPairWise = function () {
        const createdAt = $scope.createdAt;
        var req = {
            method: 'GET',
            cache: false,
            url: '/acuraciaexperimento/pairwise?createdAt=' + createdAt,
            headers: {
                'Content-Type': 'application/json'
            }
        };
        $http(req)
            .then(function (success) {

                $scope.pairWise = [0, 0, 0]
                $scope.tstConfusao = success.data.body.map(function (e) {
                    if (e.confianca > 0.9) {
                        e.class = "danger"
                        $scope.pairWise[2]++;
                    }
                    else if (e.confianca > 0.5) {
                        e.class = "warning"
                        $scope.pairWise[1]++;
                    }
                    else {
                        e.class = "success"
                        $scope.pairWise[0]++;
                    }
                    e.hide = true;
                    e.intencaoCorreta = $scope.adicionaExemplos(e.deveriaSer);
                    e.intencaoErrada = $scope.adicionaExemplos(e.foi);
                    e.erros = $scope.adicionaErros(e.deveriaSer, e.foi);
                    return e;
                });
                var ctx = document.getElementById('graphConfusaoPizza').getContext('2d');
                var graphSummaryPizza = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: $scope.pairWise,
                            backgroundColor: [
                                window.chartColors.blue,
                                window.chartColors.yellow,
                                window.chartColors.red
                            ]
                        }],
                        labels: [
                            'Bom',
                            'Atenção',
                            'Perigo'
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Sumário'
                        }
                    }
                });
                $scope.loadFlow();
            }, function (err) {
                alert(JSON.stringify(err));
                console.log(err);
                $scope.sessionExpired(err);
            })

    }

    $scope.mostraDetalhe = function (item) {
        if (item.intencaoCorreta.length < 1)
            item.intencaoCorreta = $scope.adicionaExemplos(item.deveriaSer);
        if (item.intencaoErrada.length < 1)
            item.intencaoErrada = $scope.adicionaExemplos(item.foi);
        if (item.erros.length < 1)
            item.erros = $scope.adicionaErros(item.deveriaSer, item.foi);
        item.hide = !item.hide;
    }

    $scope.loadFlow = function () {
        
        var links = $scope.tstConfusao.map( function(e) {
            return {
                real: e.deveriaSer,
                pred: e.foi,
                errors: e.qtde,
                avg_conf: e.confianca
            }
        });

        var danger = true, warning = true, good = true;
        var nodes = {};

        // Compute the distinct nodes from the links.
        links.forEach(function (link) {
            link.value = link.errors;
            link.type = link.avg_conf > 0.8 ? 'Danger' : (link.avg_conf > 0.5 ? 'Warning' : 'Good');
            link.stroke = link.avg_conf > 0.8 ? '#e86f6f' : (link.avg_conf > 0.5 ? '#e0c741' : '#1f77b4');
            link.source = nodes[link.real] || (nodes[link.real] = {
                name: link.real,
                value: link.errors
            });
            link.target = nodes[link.pred] || (nodes[link.pred] = {
                name: link.pred,
                value: link.errors
            });
        });

        links.forEach(function (link) {
            if (typeof link.real === 'undefined') {
                console.log('undefined real', link);
            }
            if (typeof link.target === 'undefined') {
                console.log('undefined target', link);
            }
        });

        function brushed() {
            var value = brush.extent()[0];

            if (d3.event.sourceEvent) {
                value = x.invert(d3.mouse(this)[1]);
                brush.extent([value, value]);
            }
            handle.attr("cy", x(value));
            var threshold = value;

            filterGraph(threshold);
        }

        var w = pageWidth() / 2,
            h = pageHeight() - 200;

        var force = d3.layout.force()
            .nodes(d3.values(nodes))
            .links(links)
            .size([w, h])
            .linkDistance(60)
            //.linkStrength(function(d) { return (d.value/21 - 0.01)/1000 })
            .charge(-300)
            .on("tick", tick)
            .start();

        var svg = d3.select(".graphContainer")
            .append("svg:svg")
            .attr("width", w)
            .attr("height", h);

        function zoomHandler() {
            svg.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
        }

        var links_g = svg.append("svg:g");

        var nodes_g = svg.append("svg:g");

        var x = d3.scale.linear()
            .domain([0, 21])
            .range([1, 100])
            .clamp(true);

        var brush = d3.svg.brush()
            .y(x)
            .extent([5, 5]);

        svg.append("svg:defs").selectAll("marker")
            .data(["end"])
            .enter().append("svg:marker")
            .attr("id", String)
            .attr("viewBox", "0 -5 10 10")
            .attr("refX", 15)
            .attr("refY", -1.5)
            .attr("markerWidth", 6)
            .attr("markerHeight", 6)
            .attr("orient", "auto")
            .append("svg:path")
            .attr("d", "M0,-5L10,0L0,5");

        var path = svg.append("svg:g")
            .selectAll("path")
            .data(force.links())
            .enter().append("svg:path")
            //                        .attr("class", function (d) {
            //                            return "link " + d.type;
            //                        })
            .style("stroke", function (d, i) {
                return d.stroke;
            })
            .attr("class", "link")
            .attr("marker-end", "url(#end)")
        //.style("stroke-width", function(d) { return (d.errors/21)*5; });

        var g = svg.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(" + (w - 50) + "," + (20) + ")")
            .call(d3.svg.axis()
                .scale(x)
                .orient("left")
                .tickFormat(function (d) { return d; })
                .tickSize(0)
                .tickPadding(12))
            .select(".domain")
            .select(function () { return this.parentNode.appendChild(this.cloneNode(true)); })
            .attr("class", "halo");

        svg.append("text")
            .attr("x", w - 15)
            .attr("y", 10)
            .attr("text-anchor", "end")
            .attr("font-size", "12px")
            .style("opacity", 0.5)
            .text("Number of Errors");

        var circle = svg.append("svg:g")
            .selectAll("circle")
            .data(force.nodes())
            .enter().append("svg:circle")
            .attr("r", 6)
            .call(force.drag);

        var text = svg.append("svg:g")
            .selectAll("g")
            .data(force.nodes())
            .enter().append("svg:g")
            .attr("class", "nodeText");

        // A copy of the text with a thick white stroke for legibility.
        text.append("svg:text")
            .attr("x", 8)
            .attr("y", ".31em")
            .attr("class", "shadow")
            .text(function (d) {
                return d.name;
            });

        text.append("svg:text")
            .attr("x", 8)
            .attr("y", ".31em")
            .text(function (d) {
                return d.name;
            });

        var slider = svg.append("svg:g")
            .attr("class", "slider")
            .call(brush);

        slider.selectAll(".extent,.resize")
            .remove();

        var handle = slider.append("circle")
            .attr("class", "handle")
            .attr("transform", "translate(" + (w - 50) + "," + (20) + ")")
            .attr("r", 6);

        brush.on("brush", brushed);

        slider.call(brush.extent([5, 5]))
            .call(brush.event);

        createTypeFilter();
        //svg.call(d3.behavior.zoom().on("zoom", zoomHandler));

        function tick() {
            path.attr("d", function (d) {
                var dx = d.target.x - d.source.x,
                    dy = d.target.y - d.source.y,
                    dr = Math.sqrt(dx * dx + dy * dy);
                return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0 1," + d.target.x + "," + d.target.y;
            });

            circle.attr("transform", function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });

            text.attr("transform", function (d) {
                return "translate(" + d.x + "," + d.y + ")";
            });
        }

        function createTypeFilter() {
            d3.select(".filterContainer").selectAll("div")
                .data(["Danger", "Warning", "Good"])
                .enter()
                .append("div")
                .attr("class", "checkbox-container")
                .append("label")
                .each(function (d) {
                    d3.select(this).append("input")
                        .attr("type", "checkbox")
                        .attr("id", function (d) {
                            return "chk_" + d;
                        })
                        .attr("checked", true)
                        .on("click", function (d, i) {
                            // register on click event
                            var lVisibility = this.checked ? "visible" : "hidden";
                            if (d === 'Danger') danger = this.checked;
                            else if (d === 'Warning') warning = this.checked;
                            else if (d === 'Good') good = this.checked;
                            filterTypeGraph(d, lVisibility);
                        });
                    d3.select(this).append("span")
                        .text(function (d) {
                            return d;
                        });
                });
            $("#sidebar").show();
        }

        function filterTypeGraph(aType, aVisibility) {
            var value = brush.extent()[0];

            if (d3.event.sourceEvent) {
                value = x.invert(d3.mouse(this)[1]);
                brush.extent([value, value]);
            }
            handle.attr("cy", x(value));
            var threshold = value;
            path.style("visibility", function (o) {
                var lOriginalVisibility = $(this).css("visibility");
                return o.type === aType && o.value >= threshold ? aVisibility : lOriginalVisibility;
            });

            circle.style("visibility", function (o, i) {
                var lHideNode = true;
                path.each(function (d, i) {
                    if ((d.source === o || d.target === o) && d.value >= threshold) {
                        if ($(this).css("visibility") === "visible") {
                            lHideNode = false;
                            d3.select(d3.selectAll(".nodeText")[0][i]).style("visibility", "visible");
                            return "visible";
                        }
                    }
                });
                if (lHideNode) {
                    d3.select(d3.selectAll(".nodeText")[0][i]).style("visibility", "hidden");
                    return "hidden";
                }
            });
        }

        function filterGraph(aValue) {
            path.style("visibility", function (o) {
                if (o.type === 'Danger' && !danger) return "hidden";
                if (o.type === 'Warning' && !warning) return "hidden";
                if (o.type === 'Good' && !good) return "hidden";

                return o.value >= aValue ? "visible" : "hidden";
            });

            circle.style("visibility", function (o, i) {
                var lHideNode = true;
                path.each(function (d, i) {
                    if (d.source === o || d.target === o) {
                        if ((!((d.type === 'Danger' && !danger) ||
                            (d.type === 'Warning' && !warning) ||
                            (d.type === 'Good' && !good))) &&
                            (($(this).css("visibility") === "visible"))) {
                            lHideNode = false;
                            d3.select(d3.selectAll(".nodeText")[0][i]).style("visibility", "visible");
                            return "visible";
                        }
                    }
                });
                if (lHideNode) {
                    d3.select(d3.selectAll(".nodeText")[0][i]).style("visibility", "hidden");
                    return "hidden";
                }
            });
        }
    }

    $scope.abrirResultado = function (createdAt) {
        $scope.createdAt = createdAt;
        $scope.loadExemplos($scope.createdAt);
        $scope.loadSummary();
        $scope.loadPairWise();

    }

}]);




function median(dados) {

    const mid = Math.floor(dados.length / 2)
    return (dados.length % 2 == 0) ? (dados[mid - 1] + dados[mid]) : dados[mid]

}

function quartile(dados) {
    const mid = Math.floor(dados.length / 2)
    return [
        median(dados.slice(0, mid)),
        median(dados),
        median((dados.length % 2 == 0) ? (dados.slice(mid)) : dados.slice(mid + 1))
    ]

}

function pageWidth() {
    var lReturn = window.innerWidth;
    if (typeof lReturn == "undefined") {
        if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientWidth != "undefined") {
            lReturn = document.documentElement.clientWidth;
        } else if (typeof document.body != "undefined") {
            lReturn = document.body.clientWidth;
        }
    }
    return lReturn;
}

function pageHeight() {
    var lReturn = window.innerHeight;
    if (typeof lReturn == "undefined") {
        if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientHeight != "undefined") {
            lReturn = document.documentElement.clientHeight;
        } else if (typeof document.body != "undefined") {
            lReturn = document.body.clientHeight;
        }
    }
    return lReturn;
}