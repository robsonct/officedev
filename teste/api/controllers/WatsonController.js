/**
 * WatsonController
 *
 * @description :: Server-side logic for managing watsons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let conversationList = [];
const Conversation = require('watson-developer-cloud/conversation/v1');


const obtemWorkspace_id = (worspaceidList, versionList, contexto) => {

    if (contexto.hasOwnProperty('workspace_id'))
        return {ws: contexto.workspace_id, av: contexto.api_version}
    let opcao = Math.floor((Math.random() * 2))
    return { ws: worspaceidList[opcao], av: versionList[opcao] };
}


module.exports = {
    message: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();
        if (req.body.hasOwnProperty('input'))
            console.log(`mensagem recebida em ${new Date()} Mensagem: ${req.body.input ? req.body.input.text : '<vazia>'}`)

        let objReturn = {};

        UtilityService.tokenValidateDataBase(req.headers)
            .then((result) => {
                let contexto = req.body.context || {};
                let usuario = '';
                let mensagem = req.body.input ? req.body.input.text : '';

                //usuário pode vir da variável dados_corretor.cod_nac dentro do contexto
                //gravo o usuário no contexto para uso posterior
                if (!contexto.hasOwnProperty('usuario')) {
                    usuario = (contexto.hasOwnProperty('dados_corretor') && contexto.dados_corretor.cod_nac) ? contexto.dados_corretor.cod_nac : 'no_user';
                    contexto.usuario = usuario;
                }
                usuario = contexto.usuario;


                let gravouInput = false;
                let workspace = obtemWorkspace_id([result.workspace_id, result.workspace_id2], [result.version_date, result.version_date2], contexto);

                let cs = new Conversation({
                    username: result.user,
                    password: result.token,
                    version_date: workspace.av
                });

                if (mensagem != null && mensagem != ''
                    && contexto.hasOwnProperty('conversation_id')) { // se não tiver conversation_id, não grava no banco
                    gravouInput = true;
                    gravarHistoricoAsync({
                        workspace_id: workspace.ws,
                        conversation_id: contexto.conversation_id,
                        text: mensagem,
                        username: usuario,
                        context: contexto,
                        input: mensagem,
                        intents: [],
                        entities: [],
                        typeAgent: 2
                    });
                }
                cs.message({
                    input: { text: mensagem },
                    workspace_id: workspace.ws,
                    alternate_intents: true,
                    context: contexto
                }, (error, response) => {
                    if (error) {
                        console.error('Erro: ' + JSON.stringify(error));
                        objReturn.status = 'error';
                        objReturn.message = 'Falha ao realizar chamada.';
                        objReturn.err = error;
                        return res.json(500, objReturn);
                    } else {
                        response.context.workspace_id = workspace.ws;
                        response.context.api_version = workspace.av;
                        

                        if (!gravouInput) { //se ainda não gravou input do cliente (pq estava sem conversation_id), grava aqui

                            gravarHistoricoAsync({
                                workspace_id: workspace.ws,
                                conversation_id: response.context.conversation_id,
                                text: mensagem,
                                username: usuario,
                                context: contexto,
                                input: mensagem,
                                intents: [],
                                entities: [],
                                typeAgent: 2
                            });
                        }
                        if (response.output.hasOwnProperty("feedback")) {
                            gravarFeedback(workspace.ws, response.context.conversation_id, response.output.feedback, {}, usuario)
                        }
                        let watsonMessage = response.output.text.join("<br>")
                        gravarHistoricoAsync({
                            workspace_id: workspace.ws,
                            conversation_id: response.context.conversation_id,
                            text: watsonMessage,
                            username: usuario,
                            context: response.context,
                            intents: response.intents,
                            entities: response.entities,
                            input: mensagem,
                            typeAgent: 1
                        })
                        response.workspace_id = workspace.ws;
                        return res.json(200, response);
                    }
                });
            })
            .catch((err) => {
                console.error(`Erro:`, err);
                objReturn.status = 'error';
                objReturn.message = 'Access denied';
                objReturn.err = err;
                return res.json(403, objReturn);
            });


    },

    feedback: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();

        let objReturn = {};



        UtilityService.tokenValidateDataBase(req.headers)
            .then((result) => {


            })
            .catch((err) => {
                console.error('Erro: ' + JSON.stringify(err));
                objReturn.status = 'error';
                objReturn.message = 'Access denied';
                objReturn.err = err;
                return res.json(403, objReturn);
            });

    },
};


gravarHistoricoAsync = (objGravacao) => {
    Historico.create(objGravacao)
        .then((data) => {
            return true;
        })
        .catch((err) => {
            console.error('Erro ao gravar histórico: ' + JSON.stringify(err));
            gravarHistoricoAsync(objGravacao);
        });
}


gravarFeedback = (workspace_id, conversation_id, feedbackAtendimento, intencao, user) => {

    let objInsert = {
        workspace_id: workspace_id,
        conversation_id: conversation_id,
        success: feedbackAtendimento,
        intents: intencao,
        username: user
    };

    Feedback.create(objInsert)
        .then(data => {
            return data;
        })
        .catch(err => {
            console.error('Erro ao gravar Feedback', err)
            return err
        });
}