'use strict';

/**
 * HistoricoController
 *
 * @description :: Server-side logic for managing Historicoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let Enumerable = require("../../node_modules/linq");
let ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');
let logger = require('tracer').colorConsole();
const moment = require("moment");

module.exports = {
    index: (req, res) => {
        res.view('conversacao/historico');
    },
    find: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        let objReturn = {};

        if (typeof req.query !== 'undefined' && req.query) {
            let conversationId = req.query.conversationId;
            let workspaceId = req.query.workspaceId;

            Historico.native(function (err, collHistorico) {
                if (err) {
                    logger.error(err);
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                }
                collHistorico.find({
                    $query: {
                        conversation_id: conversationId,
                        workspace_id: workspaceId
                    },
                    $orderby: {
                        createdAt: 1
                    },
                    $hint: {
                        conversation_id: 1, workspace_id: 1
                    }
                }).toArray(function (err, itens) {
                    if (err) {
                        logger.error(err);
                        objReturn.status = 'error';
                        objReturn.message = err.error ? err.error : err.message;
                        return res.json(500, objReturn);
                    } else {
                        objReturn.status = 'ok';
                        objReturn.message = 'Historico retornado com sucesso!';
                        objReturn.body = itens;
                        return res.json(200, objReturn);
                    }

                });

            });
        } else {
            objReturn.status = 'error';
            objReturn.message = 'Erro ao retornar historico!';
            return res.json(200, objReturn);
        }
    },


    list: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();

        let objSearch = {
        };
        if ((typeof req.body !== 'undefined' && req.body) && req.body.filtro) {
            if (req.body.filtro.data)
                objSearch.createdAt = {
                    $gte: new Date(req.body.filtro.data['>=']),
                    $lte: new Date(req.body.filtro.data['<='])
                }
            if (req.body.filtro.intencao)
                objSearch["intents.0.intent"] = req.body.filtro.intencao;
            if (req.body.filtro.entidade)
                objSearch.entities = {
                    $elemMatch: {
                        entity: req.body.filtro.entidade
                    }
                };
        }

        Historico.native(function (err, collHistorico) {
            if (err) {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            }
            collHistorico.aggregate(
                [
                    { $match: objSearch },
                    {
                        $group: {
                            "_id": {
                                workspace_id: "$workspace_id",
                                conversation_id: "$conversation_id",
                                username: "$username",
                                createdAt: { $dateToString: { format: "%Y-%m-%dT%H:%M:%S.000Z", date: "$createdAt" } }
                            }

                        }
                    }
                ], (err, result) => {
                    if (err)
                        return res.json(500, err.error ? err.error : err.message);
                    result.map((e) => {
                        e._id.createdAt = moment(e._id.createdAt);
                        return e;
                    });
                    return res.json(200, result);
                }
            );

        });
    },
    delete: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        Historico.destroy({})
            .then(result => {
                return res.json(200);
            })
            .catch(err => {
                return res.json(500, err.error ? err.error : err.message);
            });
    },
    save: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();

        let objReturn = {};

        UtilityService.tokenValidateDataBase(req.headers)
            .then(result => {

                if (typeof req.body !== 'undefined' && req.body) {
                    let workspace_id = req.body.workspace_id;
                    let conversation_id = req.body.conversation_id;
                    let texto = req.body.mensagem;
                    let tipo = req.body.tipo;
                    let user = req.session.user.email;
                    let intencao = req.body.intents || {};
                    let entidade = req.body.entities || {};
                    let info = req.body.info;

                    Historico.create({
                        workspace_id: workspace_id,
                        conversation_id: conversation_id,
                        text: texto,
                        username: user,
                        intents: intencao,
                        entities: entidade,
                        typeAgent: tipo,
                        info
                    })
                        .then(data => {
                            objReturn.status = 'ok';
                            objReturn.idObject = data.id;
                            objReturn.message = 'Histórico do conversation inserida com sucesso!';
                            return res.json(200, objReturn);
                        })
                        .catch((err) => {
                            logger.error(err);
                            objReturn.status = 'error';
                            objReturn.message = err.error ? err.error : err.message;
                            return res.json(500, objReturn);
                        });
                }

            })
            .catch((err) => {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = 'Access denied';
                return res.json(403, objReturn);
            });
    },
    
    logar: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();

        let objReturn = {};

        UtilityService.tokenValidateDataBase(req.headers)
            .then(result => {

                if (typeof req.body !== 'undefined' && req.body) {
                    let workspace_id = req.body.workspace_id;
                    let conversation_id = req.body.conversation_id;
                    let texto = req.body.mensagem;
                    let tipo = req.body.tipo;
                    let user = req.body.user;
                    let intencao = req.body.intents || {};
                    let entidade = req.body.entities || {};
                    let info = req.body.info || {};

                    Historico.create({
                        workspace_id: workspace_id,
                        conversation_id: conversation_id,
                        text: texto,
                        username: user,
                        intents: intencao,
                        entities: entidade,
                        typeAgent: tipo,
                        info
                    })
                        .then(data => {
                            objReturn.status = 'ok';
                            objReturn.idObject = data.id;
                            objReturn.message = 'Histórico do conversation inserida com sucesso!';
                            return res.json(200, objReturn);
                        })
                        .catch((err) => {
                            logger.error(err);
                            objReturn.status = 'error';
                            objReturn.message = err.error ? err.error : err.message;
                            return res.json(500, objReturn);
                        });
                }

            })
            .catch((err) => {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = 'Access denied';
                return res.json(403, objReturn);
            });
    }
};

