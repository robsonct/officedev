/**
* HomeController
*
* @description :: Server-side logic for managing Homes
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/
let _ = require('lodash');
let Conversation = require('watson-developer-cloud/conversation/v1');

module.exports = {
    find: async (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();


        let loop = true;
        let totalRegistros = 0;
        let cursor = null;

        let periodos = [];
        let start = new Date(req.body.date_start.substring(0, 10));
        let end = new Date(req.body.date_end.substring(0, 10));
        let diffDays = parseInt((end - start) / (1000 * 60 * 60 * 24));


        for (let d = 0; d <= diffDays; d++) {
            let aux = new Date(req.body.date_start.substring(0, 10));
            let startDate = new Date(aux.setTime(aux.getTime() + d * 86400000));

            let j = 1;
            // for (; j < 1440; j++) {
            for (; j < 100; j++) {
                periodos.push({ startDate: dateAdd(startDate, 'minute', j - 1).toISOString(), endDate: dateAdd(startDate, 'minute', j).toISOString() });
            }
            periodos.push({ startDate: dateAdd(startDate, 'minute', j - 1).toISOString(), endDate: dateAdd(startDate, 'minute', j - 1).toISOString().replace('00:00:00', '23:59:59') });
        }

        let promises = periodos.map((periodo) => {
            return new Promise((resolve, reject) => {
                asyncLog(periodo, req.body.user_bluemix, req.body.token_bluemix, req.body.workspace_id, cursor)
                    .then((dados) => {
                        // dadosHistorico(dados.logs)
                        //     .then((dadosParaInsert) => {
                        //         resolve(dadosParaInsert);
                        //     })
                        //     .catch((err) => {
                        //         reject(err);
                        //     });
                        resolve({});
                    })
                    .catch((err) => {
                        reject(err);
                    });
            });
        });

        Promise.all(promises)
            .then((objPromise) => {
                console.log(objPromise);
                return res.json(200, {});
            })
            .catch((err) => {
                console.log(err);
                console.log('deu ruim');
                return res.json(200, {});
            });

        // return res.json(200, {});


        // let loop = true;
        // let totalRegistros = 0;
        // let objParaInsert = [];
        // let cursor = null;
        // req.body.date_start = req.body.date_start.substring(0, 10);
        // console.log(req.body);
        // // while (loop) {
        // //     console.log(1);
        // //     let dados = await asyncLog(req, cursor);
        // //     console.log(2);
        // //     //vou aumentando o delay pra TENTAR não consumir muita memória e CPU
        // //     let dadosParaInsert = await dadosHistorico(dados.logs);
        // //     console.log(3);
        // //     objParaInsert.concat(dadosParaInsert);


        // //     totalRegistros += dados.logs.length;
        // //     console.log(totalRegistros, dados.logs[dados.logs.length - 1].request_timestamp);
        // //     if (dados.pagination.next_url) {
        // //         cursor = dados.pagination.next_url;
        // //         cursor = cursor.substring(cursor.indexOf("logs?cursor=") + 12);
        // //         cursor = cursor.substring(0, cursor.indexOf("&page_limit"));
        // //     } else {
        // //         req.body.date_start = dados.logs[dados.logs.length - 1].request_timestamp;
        // //         cursor = null;
        // //     }

        // //     if (dados.logs.length == 0 || req.body.date_start > req.body.date_end) {
        // //         loop = false;
        // //     }
        // // }
        // console.log("Pronto para importar %d registros", totalRegistros);

        // let arrAux = array_slice(objParaInsert, 0, 10);
        // console.log('arrAux', arrAux);

    }
};

function dadosHistorico(registros) {

    return new Promise((resolve, reject) => {
        let objParaInsert = [];
        let dataInicio = new Date(registros[0].request_timestamp);
        let dataFim = new Date(registros[registros.length - 1].response_timestamp);
        dataFim.setMilliseconds(1000);
        console.log('dadosHistorico.1');
        let objSearch = {
            workspace_id: registros[0].workspace_id,
            createdAt: {
                '>=': dataInicio,
                '<=': dataFim
            }
        };

        Historico
            .find(objSearch)
            .then((resultHist) => {
                console.log(resultHist);
                registros.forEach((e) => {
                    let contexto = e.request.context || {};
                    let usuario = (contexto.hasOwnProperty('dados_corretor') && contexto.dados_corretor.cod_nac) ? contexto.dados_corretor.cod_nac : 'no_user';
                    let input = (e.request.input.hasOwnProperty('text') && e.request.input.text) ? e.request.input.text : '';
                    let horaReq = new Date(e.request_timestamp);
                    let horaRes = new Date(e.response_timestamp);

                    if (!_.find(resultHist, {
                        conversation_id: e.response.context.conversation_id,
                        workspace_id: e.workspace_id,
                        createdAt: horaRes
                    })) {
                        if (input) {
                            objParaInsert.push({
                                workspace_id: e.workspace_id,
                                conversation_id: e.response.context.conversation_id,
                                text: input,
                                username: usuario,
                                context: e.response.context,
                                input: input,
                                intents: [],
                                entities: [],
                                typeAgent: 2,
                                createdAt: horaReq
                            });
                        }

                        // grava output do Watson
                        e.response.output.text.forEach((o, i) => {
                            horaRes.setMilliseconds(horaRes.getMilliseconds() + i);
                            objParaInsert.push({
                                workspace_id: e.workspace_id,
                                conversation_id: e.response.context.conversation_id,
                                text: o,
                                username: usuario,
                                context: e.response.context,
                                intents: e.response.intents,
                                entities: e.response.entities,
                                input: input,
                                typeAgent: 1,
                                createdAt: horaRes
                            });
                        });

                    }
                });
                resolve(objParaInsert);
            })
            .catch((err) => {
                reject(err)
            });

    });
}

function asyncLog(periodo, user_bluemix, token_bluemix, workspace_id, cursor) {
    return new Promise((resolve, reject) => {
        let cs = new Conversation({
            username: user_bluemix,
            password: token_bluemix,
            version_date: '2017-05-26'
        });
        let params = {
            workspace_id,
            filter: 'request_timestamp>=\'' + periodo.startDate + '\', request_timestamp<=\'' + periodo.endDate + '\'',
            sort: 'request_timestamp',
            page_limit: 100000
        };
        console.log(params);
        if (cursor) {
            params.cursor = cursor;
        }

        cs.getLogs(params, (err, response) => {
            if (err) {
                reject(err)
            }
            resolve(response)
        });
    })
}


function dateAdd(date, interval, units) {
    var ret = new Date(date);
    var checkRollover = function () {
        if (ret.getDate() != date.getDate()) ret.setDate(0);
    };
    switch (interval.toLowerCase()) {
        case 'year': ret.setFullYear(ret.getFullYear() + units); checkRollover(); break;
        case 'quarter': ret.setMonth(ret.getMonth() + 3 * units); checkRollover(); break;
        case 'month': ret.setMonth(ret.getMonth() + units); checkRollover(); break;
        case 'week': ret.setDate(ret.getDate() + 7 * units); break;
        case 'day': ret.setDate(ret.getDate() + units); break;
        case 'hour': ret.setTime(ret.getTime() + units * 3600000); break;
        case 'minute': ret.setTime(ret.getTime() + units * 60000); break;
        case 'second': ret.setTime(ret.getTime() + units * 1000); break;
        default: ret = undefined; break;
    }
    return ret;
}