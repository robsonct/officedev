/**
 * MetricaController
 *
 * @description :: Server-side logic for managing Metricas
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
'use strict';
let _ = require('lodash');
let moment = require('moment');
let logger = require('tracer').colorConsole();

module.exports = {
    load: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        let lastDays = 10;
        let objReturn = {};

        let dataFim = new Date(moment().get('year'), moment().get('month'), (moment().get('date')));
        dataFim.setUTCHours(23, 59, 59, 0);

        let dataInicio = new Date(moment().get('year'), moment().get('month'), (moment().get('date') - 1 - lastDays));
        dataInicio.setUTCHours(0, 0, 0, 0);

        let promise = Promise.resolve(0);
        let acoes = [
            gettotalChamadasAPIPorDia(dataInicio, dataFim),
            gettotalChamadasPorBrowser(dataInicio, dataFim),
            gettotalFeedbackPorDia(dataInicio, dataFim),
            gettotalIntents(dataInicio, dataFim),
            gettotalEntities(dataInicio, dataFim),
            gettotalConversas(dataInicio, dataFim)
        ];

        Promise.all(acoes).then(result => {
            let objReturn = {
                status: "ok",
                message: "Metricas retornada com sucesso!",
                result: {
                    status: "ok",
                    startDate: dataInicio,
                    endDate: dataFim
                }
            };
            result.forEach((e) => {
                if (e.totalChamadasAPIPorDia) {
                    objReturn.result.totalChamadasAPI = e.totalChamadasAPIPorDia.reduce((total, dia) => { return total + dia.count }, 0)
                    objReturn.result.totalChamadasAPIPorDia = e.totalChamadasAPIPorDia
                }
                if (e.totalChamadasPorBrowser)
                    objReturn.result.totalChamadasPorBrowser = e.totalChamadasPorBrowser
                if (e.totalFeedbackPorDia) {
                    objReturn.result.totalFeedbackPorDia = e.totalFeedbackPorDia;
                    objReturn.result.comFeedbackConversationPositivo = 0;
                    objReturn.result.comFeedbackConversationNegativo = 0;
                    e.totalFeedbackPorDia.forEach((e) => {
                        if (e._id.success)
                            objReturn.result.comFeedbackConversationPositivo += e.count;

                        if (!e._id.success)
                            objReturn.result.comFeedbackConversationNegativo += e.count;
                    });
                    objReturn.result.comFeedbackConversation = objReturn.result.comFeedbackConversationNegativo + objReturn.result.comFeedbackConversationPositivo;
                }
                if (e.totalIntents)
                    objReturn.result.arrayItencoes = e.totalIntents.reduce((total, e) => { return Object.assign(total, { [e._id.intent]: e.count }) }, {})
                if (e.totalEntities)
                    objReturn.result.arrayEntidades = e.totalEntities.reduce((total, e) => { return Object.assign(total, { [e._id]: e.count }) }, {})
                if (e.totalConversas)
                    objReturn.result.totalConversas = e.totalConversas

            })
            objReturn.result.semFeedbackConversation = objReturn.result.totalConversas - objReturn.result.comFeedbackConversation;
            return res.json(200, objReturn);
        })
            .catch((err) => {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            })
    },
    feedback: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();
        let dia = moment(req.body.dia, "DD/MM/YYYY");

        let dataFim = new Date(dia.get('year'), dia.get('month'), dia.get('date'));
        dataFim.setUTCHours(23, 59, 59, 0);

        let dataInicio = new Date(dia.get('year'), dia.get('month'), dia.get('date'));
        dataInicio.setUTCHours(0, 0, 0, 0);

        gettotalFeedbackIDsPorDia(dataInicio, dataFim, req.body.tipo)
            .then((feedbacks) => {
                getTotalIntentsbyConversationId(feedbacks.totalFeedbackPorDia)
                    .then((intents) => {
                        return res.json(200, intents);
                    })
            });
    }
}

function getTotalIntentsbyConversationId(arrConversationId) {
    return new Promise((res, rej) => {
        Historico.native((err, collectionHistorico) => {
            collectionHistorico.aggregate(
                [
                    { $match: { conversation_id: { $in: arrConversationId }, typeAgent: 1, "intents.0": { $exists: true }, "intents.0.confidence": { $gte: 0.5 } } },
                    { $project: { intent: { $arrayElemAt: ["$intents", 0] } } },
                    { $group: { _id: "$intent.intent", count: { $sum: 1 } } },
                    { $sort: { count: -1 } },
                ],
                (err, result) => {
                    res(result);
                }
            )
        })
    });
}

function gettotalFeedbackIDsPorDia(dataInicio, dataFim, tipo) {
    return new Promise(function (resolve, reject) {
        let success = (tipo === 1)
        Feedback.native(function (err, collectionFeedback) {
            collectionFeedback.distinct("conversation_id", { createdAt: { $gte: dataInicio, $lte: dataFim }, success: success },
                function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalFeedbackIDsPorDia", error: err });
                    }
                    resolve({
                        totalFeedbackPorDia: result
                    });
                })
        })
    })
}

function gettotalChamadasAPIPorDia(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim }, typeAgent: 1 } },
                    { $group: { _id: { month: { $month: "$createdAt" }, day: { $dayOfMonth: "$createdAt" }, year: { $year: "$createdAt" } }, count: { $sum: 1 } } }
                ], function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalChamadasAPIPorDia", error: err });
                    }
                    resolve({
                        totalChamadasAPIPorDia: result
                    });
                })
        })
    })
}

function gettotalChamadasPorBrowser(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim } } },
                    { $group: { _id: { browserName: "$info.browser.name", browserVersion: "$info.browser.version" }, count: { $sum: 1 } } },
                    { $sort: { count: -1 } }
                ], function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalChamadasPorBrowser", error: err });
                    }
                    let auxResult = result.map(function (item) {
                        return {
                            browser: item._id.browserName + "/" + item._id.browserVersion,
                            total: item.count
                        };
                    });
                    resolve({
                        totalChamadasPorBrowser: auxResult
                    });
                })
        })
    })
}


function gettotalFeedbackPorDia(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Feedback.native(function (err, collectionFeedback) {
            collectionFeedback.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim } } },
                    {
                        $group: {
                            _id: {
                                success: "$success",
                                month: { $month: "$createdAt" },
                                day: { $dayOfMonth: "$createdAt" },
                                year: { $year: "$createdAt" }
                            },
                            count: { $sum: 1 }
                        }
                    }
                ], function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalFeedbackPorDia", error: err });
                    }
                    resolve({
                        totalFeedbackPorDia: result
                    });
                })
        })
    })
}

function gettotalIntents(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim }, typeAgent: 1, "intents.0": { $exists: true } } },
                    { $project: { intent: { $arrayElemAt: ["$intents", 0] } } },
                    { $group: { _id: { intent: "$intent.intent" }, count: { $sum: 1 } } },
                    { $sort: { count: -1 } },
                    { $limit: 5 }
                ],
                function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    resolve({
                        totalIntents: result
                    });
                })
        })
    })
}

function gettotalEntities(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim }, typeAgent: 1, "entities.0": { $exists: true } } },
                    { $unwind: "$entities" },
                    { $group: { _id: "$entities.entity", count: { $sum: 1 } } },
                    { $sort: { count: -1 } },
                    { $limit: 5 }
                ],
                function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalEntities", error: err });
                    }
                    resolve({
                        totalEntities: result
                    });
                })
        })
    })
}

function gettotalConversas(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.distinct("conversation_id", { createdAt: { $gte: dataInicio, $lte: dataFim }, typeAgent: 1 },
                function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalConversas", error: err });
                    }
                    resolve({
                        totalConversas: result.length
                    });
                })
        })
    })
}
