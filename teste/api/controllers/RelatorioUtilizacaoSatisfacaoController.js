/**
 * RelatorioUtilizacaoSatisfacaoController
 *
 * @description :: Server-side logic for managing Relatorioutilizacaosatisfacaos
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var _ = require('lodash');
var moment = require('moment');


module.exports = {
    load: (req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();

        var objSearch = {
            sort: 'createdAt DESC'
        };
        let dataInicio = new Date();
        let dataFim = new Date();

        if (typeof req.body !== 'undefined' && req.body) {
            var busca = req.body.busca;
            objSearch.createdAt = busca;
            dataInicio = new Date(req.body.busca['>=']);
            dataFim = new Date(req.body.busca['<=']);
        }


        var objReturn = {};
        let acoes = [
            gettotalFeedbackPorDia(dataInicio, dataFim),
            getTempoMedioHistorico(dataInicio, dataFim)
        ];
        Promise.all(acoes).then(result => {
            let objReturn = {
                status: "ok",
                message: "Relatorio de utilizacao & satisfacao retornado com sucesso.",
                report: {
                    feedback: [],
                    historico: []
                }
            };
            result.forEach((e) => {
                if (e.totalFeedbackPorDia) {
                    let feedback = _.groupBy(e.totalFeedbackPorDia, (item) => {
                        return moment(new Date(item._id.year, item._id.month - 1, item._id.day)).format('DD/MM/YYYY')
                    });
                    _.keys(feedback).forEach((f) => {
                        let positivo = 0;
                        let negativo = 0;
                        feedback[f].forEach((s) => {
                            if (s._id.success)
                                positivo = s.count;
                            if (!s._id.success)
                                negativo = s.count;
                        })
                        objReturn.report.feedback.push({
                            data: f,
                            positivo: positivo,
                            negativo: negativo,
                            total: positivo + negativo
                        })
                    })
                }
                if (e.getTempos) {
                    objReturn.report.historico = e.getTempos.map((e) => {
                        return {
                            data: moment(new Date(e._id.year, e._id.month - 1, e._id.day)).format('DD/MM/YYYY'),
                            tempoMaior: millisToMinutesAndSeconds(e.tempoMaior),
                            tempoMedio: millisToMinutesAndSeconds(e.tempoMedio),
                            tempoMenor: millisToMinutesAndSeconds(e.tempoMenor)
                        }
                    })
                }
            })
            return res.json(200, objReturn);
        })
            .catch((err) => {
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                console.log(objReturn.message)
                return res.json(500, objReturn);
            })
    }
};

function getTempoMedioHistorico(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Historico.native(function (err, collectionHistorico) {
            collectionHistorico.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim } } },
                    {
                        $group: {
                            _id: {
                                conversation_id: "$conversation_id",
                                month: { $month: "$createdAt" }, day: { $dayOfMonth: "$createdAt" }, year: { $year: "$createdAt" }
                            },
                            primeiraMsg: {
                                $min: "$createdAt"
                            },
                            ultimaMsg: {
                                $max: "$createdAt"
                            },
                            count: {
                                $sum: 1
                            }
                        }
                    },
                    {
                        $project: {
                            tempo: {
                                $subtract: ["$ultimaMsg", "$primeiraMsg"]
                            },
                            msgs: "$count"
                        }
                    },
                    { $match: { msgs: { $gt: 1 } } },
                    {
                        $group: {
                            _id: {
                                month: "$_id.month",
                                day: "$_id.day",
                                year: "$_id.year"
                            },
                            tempoMenor: {
                                $min: "$tempo"
                            },
                            tempoMaior: {
                                $max: "$tempo"
                            },
                            tempoMedio: {
                                $avg: "$tempo"
                            }
                        }
                    }
                ], function (err, result) {
                    if (err) {
                        reject({ metodo: "getTempos", error: err });
                    }
                    resolve({
                        getTempos: result
                    });
                })
        })
    });
}


function gettotalFeedbackPorDia(dataInicio, dataFim) {
    return new Promise(function (resolve, reject) {
        Feedback.native(function (err, collectionFeedback) {
            collectionFeedback.aggregate(
                [
                    { $match: { createdAt: { $gte: dataInicio, $lte: dataFim } } },
                    {
                        $group: {
                            _id: {
                                success: "$success",
                                month: { $month: "$createdAt" },
                                day: { $dayOfMonth: "$createdAt" },
                                year: { $year: "$createdAt" }
                            },
                            count: { $sum: 1 }
                        }
                    }
                ], function (err, result) {
                    if (err) {
                        reject({ metodo: "gettotalFeedbackPorDia", error: err });
                    }
                    resolve({
                        totalFeedbackPorDia: result
                    });
                })
        })
    })
}

function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}