/**
 * UtilitarioController
 *
 * @description :: Server-side logic for managing relatorios
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	backup: (req, res) => {
        res.view('utilitario/backup')
    },
    log: (req, res) => {
        res.view('utilitario/log')
    },
    importar: (req, res) => {
        res.view('utilitario/importar')
    }
};

