'use strict';

/**
 * HomeController
 *
 * @description :: Server-side logic for managing Homes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let _ = require('lodash');
let Conversation = require('watson-developer-cloud/conversation/v1');
let timer = 0;

module.exports = {
    find: async(req, res) => {
        if (req.method !== 'POST')
            return res.forbidden();
        let loop = true;
        let totalRegistros = 0;
        let cursor = null;
        req.body.date_start = req.body.date_start.substring(0, 10);
        while (loop) {
            let dados = await asyncLog(req, cursor);
            //vou aumentando o delay pra TENTAR não consumir muita memória e CPU
            inserirHistorico(dados.logs);

            totalRegistros += dados.logs.length;
            console.log(totalRegistros, dados.logs[dados.logs.length - 1].request_timestamp);
            if (dados.pagination.next_url) {
                cursor = dados.pagination.next_url;
                cursor = cursor.substring(cursor.indexOf("logs?cursor=") + 12);
                cursor = cursor.substring(0, cursor.indexOf("&page_limit"));
            } else {
                req.body.date_start = dados.logs[dados.logs.length - 1].request_timestamp;
                cursor = null;
            }

            if (dados.logs.length == 0 || req.body.date_start > req.body.date_end) {
                loop = false;
            }
        }
        console.log("Pronto para importar %d registros", totalRegistros);

        return res.json(200, {});
    }
};

function inserirHistorico(registros) {
    let dataInicio = new Date(registros[0].request_timestamp);
    let dataFim = new Date(registros[registros.length - 1].response_timestamp);
    dataFim.setMilliseconds(1000);
    let objSearch = {
        workspace_id: registros[0].workspace_id,
        createdAt: {
            '>=': dataInicio,
            '<=': dataFim
        }
    };
    Historico.find(objSearch).then((resultHist) => {
        registros.forEach((e) => {
            let contexto = e.request.context || {};
            let usuario = (contexto.hasOwnProperty('dados_corretor') && contexto.dados_corretor.cod_nac) ? contexto.dados_corretor.cod_nac : 'no_user';
            let input = (e.request.input.hasOwnProperty('text') && e.request.input.text) ? e.request.input.text : '';
            let horaReq = new Date(e.request_timestamp);
            let horaRes = new Date(e.response_timestamp);
            let objSearch = {
                conversation_id: e.response.context.conversation_id,
                workspace_id: e.workspace_id,
                createdAt: horaRes
            };
            if (!_.find(resultHist, {
                    conversation_id: e.response.context.conversation_id,
                    workspace_id: e.workspace_id,
                    createdAt: horaRes
                })) {
                timer++;
                setTimeout(function() {
                    console.log('Timer: ', timer);
                    if (input) {
                        timer++;
                        Historico.create({
                                workspace_id: e.workspace_id,
                                conversation_id: e.response.context.conversation_id,
                                text: input,
                                username: usuario,
                                context: e.response.context,
                                input: input,
                                intents: [],
                                entities: [],
                                typeAgent: 2,
                                createdAt: horaReq
                            })
                            .catch((erro) => {
                                console.error("Erro ao gralet histórico %j", erro)
                            });
                    }
                    // grava output do Watson
                    e.response.output.text.forEach((o, i) => {
                        horaRes.setMilliseconds(horaRes.getMilliseconds() + i);
                        timer++;
                        Historico.create({
                                workspace_id: e.workspace_id,
                                conversation_id: e.response.context.conversation_id,
                                text: o,
                                username: usuario,
                                context: e.response.context,
                                intents: e.response.intents,
                                entities: e.response.entities,
                                input: input,
                                typeAgent: 1,
                                createdAt: horaRes
                            })
                            .catch((erro) => {
                                console.error("Erro ao gralet histórico %j", erro)
                            });
                    })
                }, timer * 100);
                //primeiro deve validar se o conversation_id já existe no worksace. Se existir, pula o registro.

            }
        })
        return true;
    });
}

function asyncLog(req, cursor) {
    return new Promise((resolve, reject) => {
        let cs = new Conversation({
            username: req.body.user_bluemix,
            password: req.body.token_bluemix,
            version_date: '2017-05-26'
        });
        let params = {
            workspace_id: req.body.workspace_id,
            filter: `request_timestamp>=${req.body.date_start},request_timestamp<=${req.body.date_end.substring(0, 10)}`,
            sort: 'request_timestamp',
            page_limit: 100000
        };
        if (cursor) {
            params.cursor = cursor;
        }

        cs.getLogs(params, function(err, response) {
            if (err) {
                return reject(err)
            }
            return resolve(response)
        });
    })
}