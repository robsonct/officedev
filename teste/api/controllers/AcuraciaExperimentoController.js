'use strict';

/**
 * AcuraciaExperimentoController
 *
 * @description :: Server-side logic for managing Acuraciaexperimentoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var Conversation = require('watson-developer-cloud/conversation/v1');
var moment = require('moment');
var experimento = require('../utils/experimento.js');
var _ = require('lodash');

module.exports = {

    delete: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        AcuraciaExperimento.destroy({})
            .then(result => {
                return res.json(200);
            })
            .catch(err => {
                return res.json(500, err.error ? err.error : err.message);
            });
    },

    pairwise: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();
        const createdAt = new Date((typeof req.query !== 'undefined' && req.query) ? req.query.createdAt : '2000-01-01');
        let objReturn = {};

        AcuraciaExperimento.native(function (err, collExperimento) {
            if (err) {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            }
            collExperimento.find({ createdAt: createdAt }, {
                "resultado.intentsList.intent": 1,
                "resultado.intentsList.confusionMatrix": 1,
                "resultado.intentsList.total": 1,
                "resultado.intentsList.threshold": 1,
                "resultado.intentsList.examples": 1
            }
            ).toArray(function (err, itens) {
                if (err) {
                    logger.error(err);
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                } else {
                    let resultado = [];
                    objReturn.status = 'ok';
                    objReturn.message = 'Experimento retornado com sucesso!';
                    itens = itens[0].resultado;
                    itens.intentsList.forEach((i, idx_i) => {
                        i.confusionMatrix.forEach((e, idx_cm) => {
                            if (e > 0 && idx_i !== idx_cm)
                                resultado.push({
                                    deveriaSer: i.intent,
                                    foi: itens.intentsList[idx_cm].intent,
                                    qtde: e,
                                    confianca: calculaConfianca(itens.intentsList[idx_cm].examples, itens.intentsList[idx_cm].intent)
                                })
                        })
                    });

                    objReturn.body = resultado;
                    return res.json(200, objReturn);
                }

            });
        });
    },


    exemplos: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();
        const createdAt = new Date((typeof req.query !== 'undefined' && req.query) ? req.query.createdAt : '2000-01-01');
        let objReturn = {};

        AcuraciaExperimento.native(function (err, collExperimento) {
            if (err) {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            }
            collExperimento.find({
                createdAt: createdAt
            }, {
                    "resultado.intentsList.intent": 1,
                    "resultado.intentsList.examples": 1
                }
            ).toArray(function (err, itens) {
                
                itens[0].resultado.intentsList.forEach((e) => {
                    e.erros = {};
                    e.examples.forEach( (err) => {
                        if (err.returnedIntent !== err.correctIntent)
                            if (e.erros.hasOwnProperty(err.returnedIntent))
                                e.erros[err.returnedIntent].push(err.example)
                            else
                                e.erros[err.returnedIntent] = [err.example]
                    });
                    e.examples = e.examples.map((i) => {return i.example})
                })
                
                if (err) {
                    logger.error(err);
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                } else {
                    objReturn.body = itens[0].resultado.intentsList;
                    return res.json(200, objReturn);
                }

            });
        });
    },

    sumario: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();
        const createdAt = new Date((typeof req.query !== 'undefined' && req.query) ? req.query.createdAt : '2000-01-01');
        let objReturn = {};

        AcuraciaExperimento.native(function (err, collExperimento) {
            if (err) {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            }
            collExperimento.find({ createdAt: createdAt }, {
                "resultado.intentsList.intent": 1,
                "resultado.intentsList.total": 1,
                "resultado.intentsList.correct": 1,
                "resultado.intentsList.threshold": 1
            }
            ).toArray(function (err, itens) {
                if (err) {
                    logger.error(err);
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                } else {
                    objReturn.status = 'ok';
                    objReturn.message = 'Experimento retornado com sucesso!';
                    objReturn.body = itens;
                    return res.json(200, objReturn);
                }

            });
        });
    },

    list: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        var objReturn = {};

        AcuraciaExperimento.native(function (err, collExperimento) {
            if (err) {
                logger.error(err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            }
            collExperimento.find({}, {
                createdAt: 1,
                "resultado.intentsNumber": 1,
                "resultado.examplesTotal": 1,
                "resultado.acuracyThreshold": 1,
                "resultado.acuracy": 1,
                "resultado.examplesCorrect": 1,
                "resultado.threshold": 1
            }
            ).sort({ createdAt: -1 }).toArray(function (err, itens) {
                if (err) {
                    logger.error(err);
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                } else {
                    objReturn.status = 'ok';
                    objReturn.message = 'Experimento retornado com sucesso!';
                    objReturn.body = itens;
                    return res.json(200, objReturn);
                }

            });
        });
    },

    execute: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        console.log('Teste iniciado');

        var objReturn = {};

        ConversationService
            .getConfig({})
            .then(obj => {
                if (_.isEmpty(obj)) {
                    objReturn.status = 'error';
                    objReturn.message = 'Erro ao executar teste. Objeto de configuração vazio.';
                    return res.json(500, objReturn);
                } else {
                    var config = obj[0];
                    experimento.init(config.user, config.token, config.workspace_id, config.version_date, 0.65);

                    experimento.runTest((err, result) => {
                        if (err) {
                            console.error('Erro ao rodar experimento', err);
                            objReturn.status = 'error';
                            objReturn.message = err.error ? err.error : err.message;
                            return res.json(500, objReturn);
                        } else {
                            AcuraciaExperimento.create({ resultado: result })
                                .then(data => {
                                    objReturn.status = 'ok';
                                    objReturn.message = 'Teste executado e inserido com sucesso';
                                    return res.json(200, objReturn);
                                })
                                .catch(err => {
                                    console.log('Erro ao executar teste', err);
                                    objReturn.status = 'error';
                                    objReturn.message = 'Erro ao executar teste';
                                    return res.json(500, objReturn);
                                });
                        }
                    });
                }
            })
            .catch(err => {
                console.log('Erro ao rodar experimento', err);
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            });

    },

    find: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        var objReturn = {};

        if (typeof req.query !== 'undefined' && req.query) {
            var createdAt = req.query.createdAt;

            var objSearch = {
                createdAt: createdAt
            };

            AcuraciaExperimento.find(objSearch)
                .then(function (result) {
                    objReturn.status = 'ok';
                    objReturn.message = 'Teste retornado com sucesso!';
                    objReturn.body = result;
                    /*
                    objReturn.body.forEach((e) => {
                        e.resultado.intentsList.forEach((i) => {
                            delete i.examples;                            
                        });
                    });
                    */

                    return res.json(200, objReturn);
                })
                .catch(function (err) {
                    console.log('Erro ao retornar Teste!');
                    objReturn.status = 'error';
                    objReturn.message = err.error ? err.error : err.message;
                    return res.json(500, objReturn);
                });
        } else {
            objReturn.status = 'error';
            objReturn.message = 'Erro ao retornar Teste!';
            return res.json(200, objReturn);
        }
    }
};


function calculaConfianca(matriz, foi) {
    let confianca = 0;
    let total = 0;
    matriz.forEach((e) => {
        if (e.returnedIntent === foi) {
            confianca += e.confidence;
            total++;
        }
    })
    return confianca / total;
}