'use strict';

/**
 * ExternalController
 *
 * @description :: Server-side logic for managing Externals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Conversation = require('watson-developer-cloud/conversation/v1');
var uuid = require('uuid');

module.exports = {

    configConversationLoad: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        console.log('configConversationLoad');

        var objReturn = {};

        ConversationService
            .getConfig({})
            .then((obj) => {

                UtilityService
                    .tokenValidateDataBase(req.headers)
                    .then((result) => {
                        objReturn.status = 'ok';
                        objReturn.message = 'Configuração retornada com sucesso!';
                        objReturn.body = obj;
                        return res.json(200, objReturn);
                    })
                    .catch(function (err) {
                        objReturn.status = 'error';
                        objReturn.message = 'Access denied.';
                        return res.json(403, objReturn);
                    });
            })
            .catch(function (err) {
                objReturn.status = 'error';
                objReturn.message = err.error ? err.error : err.message;
                return res.json(500, objReturn);
            });
    },

    configToneAnalyzerLoad: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        console.log('loadConfigToneAnalyzer');

        var objReturn = {};
        UtilityService
            .tokenValidateDataBase(req.headers)
            .then((resToken) => {

                ConfigToneAnalyzer.find({})
                    .then((result) => {
                        objReturn.body = result;
                        if (typeof result === 'object') {
                            if (result == null) {
                                console.log('Result is null.');
                                objReturn.status = 'error';
                                objReturn.message = 'Result is null.';
                                return res.json(500, objReturn);
                            } else {
                                console.log('Configuração retornada com sucesso!');
                                objReturn.status = 'ok';
                                objReturn.message = 'Configuração retornada com sucesso!';
                                return res.json(200, objReturn);
                            }
                        } else {
                            console.log('Result is not object.');
                            objReturn.status = 'error';
                            objReturn.message = 'Result is not object.';
                            return res.json(500, objReturn);
                        }
                    })
                    .catch(function (err) {
                        console.log('Erro geral!', err);
                        objReturn.status = 'error';
                        objReturn.message = err.error ? err.error : err.message;
                        return res.json(500, objReturn);
                    });
            })
            .catch(function (err) {
                objReturn.status = 'error';
                objReturn.message = 'Access denied.';
                return res.json(403, objReturn);
            });
    },

    hash: (req, res) => {
        if (req.method !== 'GET')
            return res.forbidden();

        var objReturn = {
            status: "ok",
            message: "Hash gerado com sucesso.",
            hash: uuid.v1()
        };

        console.log(objReturn);
        return res.json(200, objReturn);
    }
};