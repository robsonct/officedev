/**
 * Connections
 * (sails.config.connections)
 *
 * `Connections` are like "saved settings" for your adapters.  What's the difference between
 * a connection and an adapter, you might ask?  An adapter (e.g. `sails-mysql`) is generic--
 * it needs some additional information to work (e.g. your database host, password, user, etc.)
 * A `connection` is that additional information.
 *
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 * .
 * Note: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 * (this is to prevent you inadvertently sensitive credentials up to your repository.)
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.connections.html
 */

var cfenv = require('cfenv');
var appEnv = cfenv.getAppEnv();


//BD DEV
appEnv.services['compose-for-mongodb'] = [{
    "credentials": {
        "db_type": "mongodb",
        "maps": [],
        "name": "xxxxxxxxx",
        "uri_cli": "mongo --ssl --sslAllowInvalidCertificates sl-us-south-1-portal.9.dblayer.com:25412/admin -u admin -p OZXEPVJIZYTYRMFW",
        "ca_certificate_base64": "FhIZnlsYWpUcEFaCkhNYVN5eU9HV2ZIUXpkczRxVjZiTFhJTTJaUmg4K0x4MjJnYXlhYzZHUG5YeStnK0hsUVFEV1pEU254VWY4SW8KNlg1L3Fmd1Z6SkVHaXEzalpBPT0KLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=",
        "deployment_id": "xxxxxxxx",
        "uri": "xxxxxx"
    },
    "syslog_drain_url": null,
    "volume_mounts": [],
    "label": "compose-for-mongodb",
    "provider": null,
    "plan": "Standard",
    "name": "Mongodb Orquestrador Prod",
    "tags": [
        "big_data",
        "data_management",
        "ibm_created"
    ]
}];


module.exports.connections = {

    /***************************************************************************
    *                                                                          *
    * Local disk storage for DEVELOPMENT ONLY                                  *
    *                                                                          *
    * Installed by default.                                                    *
    *                                                                          *
    ***************************************************************************/
    // localDiskDb: {
    //   adapter: 'sails-disk'
    // },

    /***************************************************************************
    *                                                                          *
    * MySQL is the world's most popular relational database.                   *
    * http://en.wikipedia.org/wiki/MySQL                                       *
    *                                                                          *
    * Run: npm install sails-mysql                                             *
    *                                                                          *
    ***************************************************************************/
    // someMysqlServer: {
    //   adapter: 'sails-mysql',
    //   host: 'YOUR_MYSQL_SERVER_HOSTNAME_OR_IP_ADDRESS',
    //   user: 'YOUR_MYSQL_USER', //optional
    //   password: 'YOUR_MYSQL_PASSWORD', //optional
    //   database: 'YOUR_MYSQL_DB' //optional
    // },

    /***************************************************************************
    *                                                                          *
    * MongoDB is the leading NoSQL database.                                   *
    * http://en.wikipedia.org/wiki/MongoDB                                     *
    *                                                                          *
    * Run: npm install sails-mongo                                             *
    *                                                                          *
    ***************************************************************************/
    // someMongodbServer: {
    //   adapter: 'sails-mongo',
    //   host: 'localhost',
    //   port: 27017,
    //   user: 'username', //optional
    //   password: 'password', //optional
    //   database: 'your_mongo_db_name_here' //optional
    // },


    mongodbServer: {
        adapter: 'sails-mongo',
        url: appEnv.services['compose-for-mongodb'][0].credentials['uri'],
        ssl: true,
        sslValidate: false,
        sslCA: appEnv.services['compose-for-mongodb'][0].credentials['ca_certificate_base64']
    },

    //mongodbServer: {
    //  adapter: 'sails-mongo',
    //  user: 'chatadmin',
    //  password: 'chat1234',
    //  host: 'ds157459.mlab.com',
    //  port: 57459,
    //  database: 'chattools'
    //},

    /***************************************************************************
    *                                                                          *
    * PostgreSQL is another officially supported relational database.          *
    * http://en.wikipedia.org/wiki/PostgreSQL                                  *
    *                                                                          *
    * Run: npm install sails-postgresql                                        *
    *                                                                          *
    *                                                                          *
    ***************************************************************************/
    // somePostgresqlServer: {
    //   adapter: 'sails-postgresql',
    //   host: 'YOUR_POSTGRES_SERVER_HOSTNAME_OR_IP_ADDRESS',
    //   user: 'YOUR_POSTGRES_USER', // optional
    //   password: 'YOUR_POSTGRES_PASSWORD', // optional
    //   database: 'YOUR_POSTGRES_DB' //optional
    // }


    /***************************************************************************
    *                                                                          *
    * More adapters: https://github.com/balderdashy/sails                      *
    *                                                                          *
    ***************************************************************************/

};
